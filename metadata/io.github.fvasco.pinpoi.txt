Categories:Navigation
License:GPLv3
Author Name:Francesco Vasco
Author Email:fsco_v-github@yahoo.it
Web Site:
Source Code:https://github.com/fvasco/pinpoi
Issue Tracker:https://github.com/fvasco/pinpoi/issues

Auto Name:PinPoi
Summary:Import and manage point of interest
Description:
PinPoi can import and search in a thousand point of interests of your preferred
gps navigator. Import KML, KMZ, OV2, GPX, ASC, CSV and zipped collections
directly into your phone and organize it in collections. PinPoi can search using
your gps position or a custom location, you can choose your destination and open
it with preferred app.
.

Repo Type:git
Repo:https://github.com/fvasco/pinpoi.git

Build:0.1,1
    commit=0.1.0
    subdir=app
    gradle=yes

Build:0.1.1,2
    commit=0.1.1
    subdir=app
    gradle=yes

Build:0.1.2,3
    commit=0.1.2
    subdir=app
    gradle=yes

Build:0.2.0,4
    commit=0.2.0
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.2.0
Current Version Code:4
