Categories:System
License:GPLv3+
Web Site:
Source Code:https://github.com/meefik/busybox
Issue Tracker:https://github.com/meefik/busybox/issues
Changelog:https://github.com/meefik/busybox/blob/HEAD/CHANGELOG
Donate:http://meefik.github.io/donate/

Auto Name:BusyBox
Summary:Install BusyBox
Description:
Install a recent and un-crippled version of BusyBox.

Warning: This is an untested build!
.

Repo Type:git
Repo:https://github.com/meefik/busybox

Build:1.24.1,18
    disable=fails on a 32-bit NDK
    commit=610d680fd603dc7b0a5c3a2d30f6d48e89811c0c
    subdir=app
    gradle=yes
    scandelete=app/src/main/assets
    build=cd ../contrib && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        ./bb-build.sh arm pie && \
        ./bb-build.sh arm && \
        cp -fR compiled/arm ../app/src/main/assets/ && \
        ./bb-build.sh intel pie && \
        ./bb-build.sh intel && \
        cp -fR compiled/intel ../app/src/main/assets/ && \
        ./bb-build.sh mips pie && \
        ./bb-build.sh mips && \
        cp -fR compiled/mips ../app/src/main/assets/

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.24.1
Current Version Code:18
