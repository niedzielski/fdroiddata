AntiFeatures:Tracking
Categories:Connectivity
License:GPLv3
Web Site:http://www.disruptedsystems.org
Source Code:https://github.com/Marlinski/Rumble
Issue Tracker:https://github.com/Marlinski/Rumble/issues

Auto Name:Rumble
Summary:Off-the-grid micro-blogging application for communities
Description:
Rumble is a completely off-the-grid application and delay-tolerant
micro-blogging application that allows a device to connect, chat and share
content (text and images) with other people around you. It doesn't require
Internet to work and rely instead on Smartphone, Local Area Network (Bluetooth
and Wifi) and human mobility to spread information.

Using only the social fabric as an infrastructure, it makes Rumble suitable for
use case where the Internet is either best to avoid (surveillance/censorship) or
simply not available.
.

Repo Type:git
Repo:https://github.com/Marlinski/Rumble

Build:1.0.1,7
    commit=v1.0.1-7
    subdir=app
    gradle=yes
    srclibs=SlidingMenu@4254feca3ece9397cd501921ee733f19ea0fdad8
    prebuild=mkdir -p ../libs/SlidingMenu/ && \
        cp -fR $$SlidingMenu$$ ../libs/SlidingMenu/library

Build:1.0.1,11
    commit=v1.0.1-11
    subdir=app
    submodules=yes
    gradle=yes
    rm=libs/SlidingMenu/example

Auto Update Mode:Version v%v-%c
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:11
